/**
  ******************************************************************************
  * File Name          : SPI.c
  * Description        : This file provides code for the configuration
  *                      of the SPI instances.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "spi.h"

#include "gpio.h"

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

SPI_HandleTypeDef hspi2;

/* SPI2 init function */
void MX_SPI2_Init(void)
{

  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 7;
  hspi2.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi2.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

void HAL_SPI_MspInit(SPI_HandleTypeDef* spiHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct;
  if(spiHandle->Instance==SPI2)
  {
  /* USER CODE BEGIN SPI2_MspInit 0 */

  /* USER CODE END SPI2_MspInit 0 */
    /* SPI2 clock enable */
    __HAL_RCC_SPI2_CLK_ENABLE();
  
    /**SPI2 GPIO Configuration    
    PD1     ------> SPI2_SCK
    PD3     ------> SPI2_MISO
    PD4     ------> SPI2_MOSI 
    */
    GPIO_InitStruct.Pin = MEMS_SCK_Pin|MEMS_MISO_Pin|MEMS_MOSI_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /* USER CODE BEGIN SPI2_MspInit 1 */

  /* USER CODE END SPI2_MspInit 1 */
  }
}

void HAL_SPI_MspDeInit(SPI_HandleTypeDef* spiHandle)
{

  if(spiHandle->Instance==SPI2)
  {
  /* USER CODE BEGIN SPI2_MspDeInit 0 */

  /* USER CODE END SPI2_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_SPI2_CLK_DISABLE();
  
    /**SPI2 GPIO Configuration    
    PD1     ------> SPI2_SCK
    PD3     ------> SPI2_MISO
    PD4     ------> SPI2_MOSI 
    */
    HAL_GPIO_DeInit(GPIOD, MEMS_SCK_Pin|MEMS_MISO_Pin|MEMS_MOSI_Pin);

  /* USER CODE BEGIN SPI2_MspDeInit 1 */

  /* USER CODE END SPI2_MspDeInit 1 */
  }
} 

/* USER CODE BEGIN 1 */
/**********************************************************************************
*								AKCELEROMETR									  *
**********************************************************************************/
void AccRead(uint8_t* pBuffer,uint8_t ReadAddr, uint16_t ByteToRead)
{
  ACC_CS_LOW;

  uint8_t cmd[ByteToRead+1];// = {READWRITE_CMD | WriteAddr, pBuffer[0]};
  cmd[0] = ACC_READWRITE | ACC_MULTIPLEBYTE | ReadAddr;
  for(int i = 1; i <= ByteToRead; i++)
	  cmd[i] = ACC_FAKE_BYTE;

  HAL_SPI_TransmitReceive(&hspi2, cmd, pBuffer, ByteToRead+1, ACC_FLAG_TIMEOUT);

  ACC_CS_HIGH;
}

void AccWrite(uint8_t buffer, uint8_t WriteAddr)
{
  ACC_CS_LOW;

  uint8_t cmd[2] = {0x00 | WriteAddr, buffer};
  HAL_SPI_Transmit(&hspi2 ,cmd, 2, ACC_FLAG_TIMEOUT);

  ACC_CS_HIGH;
}

int16_t AccGetX()
{
	uint8_t xbuff[3] = {0,0,0};
	int16_t xdata = 0;

	AccRead(xbuff, ACC_OUT_X_L_ADDR, 2);
	xdata = (int16_t)((uint16_t)(xbuff[2] << 8) + xbuff[1] );
	return xdata;
}

int16_t AccGetY()
{
	uint8_t ybuff[3] = {0,0,0};
	int16_t ydata = 0;

	AccRead(ybuff, ACC_OUT_Y_L_ADDR, 2);
	ydata = (int16_t)((uint16_t)(ybuff[2] << 8) + ybuff[1] );
	return ydata;
}

int16_t AccGetZ()
{
	uint8_t zbuff[3] = {0,0,0};
	int16_t zdata = 0;

	AccRead(zbuff, ACC_OUT_Z_L_ADDR, 2);
	zdata = (int16_t)((uint16_t)(zbuff[2] << 8) + zbuff[1] );
	return zdata;
}



/**********************************************************************************
*								  ZYROSKOP									      *
**********************************************************************************/
void GyroRead(uint8_t* pBuffer, uint8_t ReadAddr, uint16_t ByteToRead)
{
  GYRO_CS_LOW;

  uint8_t cmd[ByteToRead+1];// = {READWRITE_CMD | WriteAddr, pBuffer[0]};
  cmd[0] = GYRO_READWRITE | GYRO_MULTIPLEBYTE | ReadAddr;
  for(int i = 1; i <= ByteToRead; i++)
	  cmd[i] = GYRO_FAKE_BYTE;

  HAL_SPI_TransmitReceive(&hspi2, cmd, pBuffer, ByteToRead+1, GYRO_FLAG_TIMEOUT);

  GYRO_CS_HIGH;
}

void GyroWrite(uint8_t buffer, uint8_t WriteAddr)
{
  GYRO_CS_LOW;

  uint8_t cmd[2] = {0x00 | WriteAddr, buffer};
  HAL_SPI_Transmit(&hspi2 ,cmd, 2, GYRO_FLAG_TIMEOUT);

  GYRO_CS_HIGH;
}

int16_t GyroGetX()
{
	uint8_t xbuff[3] = {0,0,0};
	int16_t xdata = 0;

	GyroRead(xbuff, GYRO_OUT_X_L_ADDR, 2);
	xdata = (int16_t)((uint16_t)(xbuff[2] << 8) + xbuff[1] );
	return xdata;
}

int16_t GyroGetY()
{
	uint8_t ybuff[3] = {0,0,0};
	int16_t ydata = 0;

	GyroRead(ybuff, GYRO_OUT_Y_L_ADDR, 2);
	ydata = (int16_t)((uint16_t)(ybuff[2] << 8) + ybuff[1] );
	return ydata;
}

int16_t GyroGetZ()
{
	uint8_t zbuff[3] = {0,0,0};
	int16_t zdata = 0;

	GyroRead(zbuff, GYRO_OUT_Z_L_ADDR, 2);
	zdata = (int16_t)((uint16_t)(zbuff[2] << 8) + zbuff[1] );
	return zdata;
}



/* USER CODE END 1 */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

/**
  ******************************************************************************
  * File Name          : SPI.h
  * Description        : This file provides code for the configuration
  *                      of the SPI instances.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __spi_H
#define __spi_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "main.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

extern SPI_HandleTypeDef hspi2;

/* USER CODE BEGIN Private defines */


/**********************************************************************************
*								AKCELEROMETR									  *
**********************************************************************************/
#define ACC_CS_LOW		HAL_GPIO_WritePin(XL_CS_GPIO_Port, XL_CS_Pin, GPIO_PIN_RESET)
#define ACC_CS_HIGH		HAL_GPIO_WritePin(XL_CS_GPIO_Port, XL_CS_Pin, GPIO_PIN_SET)

#define ACC_WHO_AM_I_ADD		0x0F  // device identification register
#define ACC_CTRL_REG1_ADDR		0x20  // Control register 1
#define ACC_OUT_X_L_ADDR		0x28  // Output register X
#define ACC_OUT_X_H_ADDR		0x29  // Output register X
#define ACC_OUT_Y_L_ADDR		0x2A  // Output register Y
#define ACC_OUT_Y_H_ADDR		0x2B  // Output register Y
#define ACC_OUT_Z_L_ADDR		0x2C  // Output register Z
#define ACC_OUT_Z_H_ADDR		0x2D  // Output register Z

#define I_AM_ACC			((uint8_t)0x41)

#define ACC_X_ENABLE		((uint8_t)0x02)
#define ACC_Y_ENABLE		((uint8_t)0x01)
#define ACC_Z_ENABLE		((uint8_t)0x04)
#define ACC_AXES_ENABLE		((uint8_t)0x07)
#define ACC_AXES_DISABLE	((uint8_t)0x00)

/* Read/Write command */
#define ACC_READWRITE		((uint8_t)0x80)
/* Multiple byte read/write command */
#define ACC_MULTIPLEBYTE	((uint8_t)0x40)
/* Dummy Byte Send by the SPI Master device in order to generate the Clock to the Slave device */
#define ACC_FAKE_BYTE		((uint8_t)0x00)

#define ACC_FLAG_TIMEOUT	((uint32_t)0x1000)



/**********************************************************************************
*								  ZYROSKOP									      *
**********************************************************************************/
#define GYRO_CS_LOW		HAL_GPIO_WritePin(GYRO_CS_GPIO_Port, GYRO_CS_Pin, GPIO_PIN_RESET)
#define GYRO_CS_HIGH	HAL_GPIO_WritePin(GYRO_CS_GPIO_Port, GYRO_CS_Pin, GPIO_PIN_SET)

#define GYRO_WHO_AM_I_ADD		0x0F  // device identification register
#define GYRO_CTRL_REG1_ADDR		0x20  // Control register 1
#define GYRO_OUT_X_L_ADDR		0x28  // Output register X
#define GYRO_OUT_X_H_ADDR		0x29  // Output register X
#define GYRO_OUT_Y_L_ADDR		0x2A  // Output register Y
#define GYRO_OUT_Y_H_ADDR		0x2B  // Output register Y
#define GYRO_OUT_Z_L_ADDR		0x2C  // Output register Z
#define GYRO_OUT_Z_H_ADDR		0x2D  // Output register Z

#define I_AM_GYRO				((uint8_t)0xD4)

#define GYRO_X_ENABLE		((uint8_t)0x02)
#define GYRO_Y_ENABLE		((uint8_t)0x01)
#define GYRO_Z_ENABLE		((uint8_t)0x04)
#define GYRO_AXES_ENABLE	((uint8_t)0x07)
#define GYRO_AXES_DISABLE	((uint8_t)0x00)

/* Read/Write command */
#define GYRO_READWRITE		((uint8_t)0x80)
/* Multiple byte read/write command */
#define GYRO_MULTIPLEBYTE	((uint8_t)0x40)
/* Dummy Byte Send by the SPI Master device in order to generate the Clock to the Slave device */
#define GYRO_FAKE_BYTE		((uint8_t)0x00)

#define GYRO_FLAG_TIMEOUT	((uint32_t)0x1000)


/* USER CODE END Private defines */

extern void _Error_Handler(char *, int);

void MX_SPI2_Init(void);

/* USER CODE BEGIN Prototypes */

/**********************************************************************************
*								AKCELEROMETR									  *
**********************************************************************************/
void AccWrite(uint8_t buffer, uint8_t WriteAddr);
void AccRead(uint8_t* pBuffer, uint8_t ReadAddr, uint16_t NumByteToRead);

int16_t AccGetX();
int16_t AccGetY();
int16_t AccGetZ();


/**********************************************************************************
*								  ZYROSKOP									      *
**********************************************************************************/
void GyroWrite(uint8_t buffer, uint8_t WriteAddr);
void GyroRead(uint8_t* pBuffer, uint8_t ReadAddr, uint16_t NumByteToRead);

int16_t GyroGetX();
int16_t GyroGetY();
int16_t GyroGetZ();


/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif
#endif /*__ spi_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

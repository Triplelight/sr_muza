################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/dma.c \
../Src/gpio.c \
../Src/lcd.c \
../Src/main.c \
../Src/spi.c \
../Src/stm32l4xx_hal_msp.c \
../Src/stm32l4xx_it.c \
../Src/system_stm32l4xx.c \
../Src/usb_device.c \
../Src/usbd_conf.c \
../Src/usbd_desc.c 

OBJS += \
./Src/dma.o \
./Src/gpio.o \
./Src/lcd.o \
./Src/main.o \
./Src/spi.o \
./Src/stm32l4xx_hal_msp.o \
./Src/stm32l4xx_it.o \
./Src/system_stm32l4xx.o \
./Src/usb_device.o \
./Src/usbd_conf.o \
./Src/usbd_desc.o 

C_DEPS += \
./Src/dma.d \
./Src/gpio.d \
./Src/lcd.d \
./Src/main.d \
./Src/spi.d \
./Src/stm32l4xx_hal_msp.d \
./Src/stm32l4xx_it.d \
./Src/system_stm32l4xx.d \
./Src/usb_device.d \
./Src/usbd_conf.d \
./Src/usbd_desc.d 


# Each subdirectory must supply rules for building sources it contributes
Src/%.o: ../Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32L476xx -I"C:/Users/Marlena/Desktop/SR/mysz_usb/Inc" -I"C:/Users/Marlena/Desktop/SR/mysz_usb/Drivers/STM32L4xx_HAL_Driver/Inc" -I"C:/Users/Marlena/Desktop/SR/mysz_usb/Drivers/STM32L4xx_HAL_Driver/Inc/Legacy" -I"C:/Users/Marlena/Desktop/SR/mysz_usb/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"C:/Users/Marlena/Desktop/SR/mysz_usb/Middlewares/ST/STM32_USB_Device_Library/Class/HID/Inc" -I"C:/Users/Marlena/Desktop/SR/mysz_usb/Drivers/CMSIS/Device/ST/STM32L4xx/Include" -I"C:/Users/Marlena/Desktop/SR/mysz_usb/Drivers/CMSIS/Include" -I"C:/Users/Marlena/Desktop/SR/mysz_usb/Drivers/BSP"  -O2 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



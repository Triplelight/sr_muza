
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * This notice applies to any and all portions of this file
 * that are not between comment pairs USER CODE BEGIN and
 * USER CODE END. Other portions of this file, whether
 * inserted by the user or by software development tools
 * are owned by their respective copyright owners.
 *
 * Copyright (c) 2018 STMicroelectronics International N.V.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted, provided that the following conditions are met:
 *
 * 1. Redistribution of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of STMicroelectronics nor the names of other
 *    contributors to this software may be used to endorse or promote products
 *    derived from this software without specific written permission.
 * 4. This software, including modifications and/or derivative works of this
 *    software, must execute solely and exclusively on microcontroller or
 *    microprocessor devices manufactured by or for STMicroelectronics.
 * 5. Redistribution and use of this software other than as permitted under
 *    this license is void and will automatically terminate your rights under
 *    this license.
 *
 * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
 * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT
 * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32l4xx_hal.h"
#include "dma.h"
#include "lcd.h"
#include "spi.h"
#include "usb_device.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "stm32l476g_discovery_glass_lcd.h"
#include "stm32l476g_discovery.h"
#include "usbd_hid.h"
#include <stdio.h>
#include <stdbool.h>
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

#define CURSOR_STEP		10
#define WHEEL_STEP		2
#define LEFT_CLICK		1
#define RIGHT_CLICK		2
#define DO_NOTHING		0
#define THRESHOLD		1500

uint8_t DPI=0;

// flagi do przerwan od przyciskow Joysticku
bool stateFlag = false;			//do joyCenter do zmiany trybow: myszka/menu
bool menuUpFlag = false;		//do joyUp przy uzywaniu menu
bool menuDownFlag = false;		//do joyDown przy uzywaniu menu
bool mouseLeftFlag = false;		//do joyLeft jako LPM
bool mouseRightFlag = false;	//do joyRight jako PPM
bool mouseUpFlag = false;		//do joyUp jako scroll w gore
bool mouseDownFlag = false; 	//do joyDown jako scroll w dol

// zmienne do odczytu wasrtosci z zyroskopu  i akcelerometru (uzywane do podgladu wartosci w STMStudip
int16_t xGyro = 0;
int16_t yGyro = 0;
int16_t zGyro = 0;
int16_t xAcc = 0;
int16_t yAcc = 0;
int16_t zAcc = 0;


// zmienne do odczytu wartosci z rejestru who am i
uint8_t whoIAmAcc = 0x00;
uint8_t whoIAmGyro = 0x00;

// zmienna do odczytu aktualnego trybu pracy myszy
uint8_t actualMode;

// zmienna do odczytu aktulnego trybu pracy plytki: mysz/menu
uint8_t actualState;

struct mouseHID_t
{
	uint8_t buttons;
	int8_t x;
	int8_t y;
	int8_t wheel;
};

struct mouseHID_t mouseHID;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin == JOY_CENTER_Pin)
	{
		stateFlag = !stateFlag;
	}
	else if(GPIO_Pin == JOY_UP_Pin)
	{
		if(actualState == STATE_MAIN_MENU)
			menuUpFlag = true;
		else if(actualState == STATE_MOUSE)
			mouseUpFlag = true;
	}
	else if(GPIO_Pin == JOY_DOWN_Pin)
	{
		if(actualState == STATE_MAIN_MENU)
			menuDownFlag = true;
		else if(actualState == STATE_MOUSE)
			mouseDownFlag = true;
	}
	else if(GPIO_Pin == JOY_LEFT_Pin  &&  actualState == STATE_MOUSE)
	{
		mouseLeftFlag = true;
	}
	else if(GPIO_Pin == JOY_RIGHT_Pin  &&  actualState == STATE_MOUSE)
	{
		mouseRightFlag = true;
	}
}

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 *
 * @retval None
 */
int main(void)
{
	/* USER CODE BEGIN 1 */



	mouseHID.buttons=0;
	mouseHID.x=0;
	mouseHID.y=0;
	mouseHID.wheel=0;
	/* USER CODE END 1 */

	/* MCU Configuration----------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_LCD_Init();
	MX_SPI2_Init();
	MX_USB_DEVICE_Init();
	/* USER CODE BEGIN 2 */
	BSP_LCD_GLASS_Init();
	ACCELERO_IO_Init();
	GYRO_IO_Init();


	AccConfig();
	GyroConfig();

	MenuInit();


	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1)
	{


		if(stateFlag)
		{
			HAL_GPIO_WritePin(LD_G_GPIO_Port, LD_G_Pin, GPIO_PIN_SET);
			SetMenuState(STATE_MAIN_MENU);
			actualState = GetMenuState();

			selectDPI();
		}
		else
		{
			HAL_GPIO_WritePin(LD_G_GPIO_Port, LD_G_Pin, GPIO_PIN_RESET);
			SetMenuState(STATE_MOUSE);
			actualState = GetMenuState();

			mouseHandling();
			HAL_Delay(10);
		}
		DisplayMenu();

		//odkomentowac jesli chcemy podejrzec odczyty w STMStudio
		/*
		whoIAmAcc=WhoIsAcc();
		whoIAmGyro=WhoIsGyro();

		xAcc=AccGetX();
		//HAL_Delay(10);
		yAcc=AccGetY();
		//HAL_Delay(10);
		zAcc=AccGetZ();
		//HAL_Delay(10);

		xGyro=GyroGetX();
		//HAL_Delay(10);
		yGyro=GyroGetY();
		//HAL_Delay(10);
		zGyro=GyroGetZ();
		//HAL_Delay(10);

		 */



		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */

}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void)
{

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_PeriphCLKInitTypeDef PeriphClkInit;

	/**Configure LSE Drive Capability
	 */
	HAL_PWR_EnableBkUpAccess();

	__HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_LSE
			|RCC_OSCILLATORTYPE_MSI;
	RCC_OscInitStruct.LSEState = RCC_LSE_ON;
	RCC_OscInitStruct.LSIState = RCC_LSI_ON;
	RCC_OscInitStruct.MSIState = RCC_MSI_ON;
	RCC_OscInitStruct.MSICalibrationValue = 0;
	RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
	RCC_OscInitStruct.PLL.PLLM = 1;
	RCC_OscInitStruct.PLL.PLLN = 24;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
			|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_USB;
	PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
	PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLLSAI1;
	PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_MSI;
	PeriphClkInit.PLLSAI1.PLLSAI1M = 1;
	PeriphClkInit.PLLSAI1.PLLSAI1N = 24;
	PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
	PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;
	PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
	PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_48M2CLK;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Configure the main internal regulator output voltage
	 */
	if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Configure the Systick interrupt time
	 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

	/**Configure the Systick
	 */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/**Enable MSI Auto calibration
	 */
	HAL_RCCEx_EnableMSIPLLMode();

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

void selectDPI()
{
	if(menuUpFlag)
	{
		MenuUp();
		actualMode = GetMenuMode();
		menuUpFlag = false;
	}
	else if(menuDownFlag)
	{
		MenuDown();
		actualMode = GetMenuMode();
		menuDownFlag = false;
	}
}

void mouseHandling()
{
	switch(actualMode)
	{
	case MODE_SLOW:
		DPI = CURSOR_STEP;
		break;
	case MODE_NORMAL:
		DPI = CURSOR_STEP*1.5;
		break;
	case MODE_FAST:
		DPI = CURSOR_STEP*2;
		break;
	}


	//ruch prawo/lewo
	if(AccGetX() > THRESHOLD)
		mouseHID.x = DPI;
	else if(AccGetX() < -THRESHOLD)
		mouseHID.x = -DPI;
	else
		mouseHID.x = DO_NOTHING;

	//ruch gora/dol
	if(AccGetY() > THRESHOLD)
		mouseHID.y = DPI;
	else if(AccGetY() < -THRESHOLD)
		mouseHID.y = -DPI;
	else
		mouseHID.y = DO_NOTHING;

	//lewy klik
	if(!mouseLeftFlag && !mouseRightFlag)
		mouseHID.buttons = DO_NOTHING;
	else if(mouseLeftFlag)
	{
		if(HAL_GPIO_ReadPin(JOY_LEFT_GPIO_Port, JOY_LEFT_Pin) == GPIO_PIN_SET)
			mouseHID.buttons = LEFT_CLICK;
		else
		{
			mouseHID.buttons = DO_NOTHING;
			mouseLeftFlag = false;
		}
	}

	//prawy klik
	else if(mouseRightFlag)
	{
		if(HAL_GPIO_ReadPin(JOY_RIGHT_GPIO_Port, JOY_RIGHT_Pin) == GPIO_PIN_SET)
			mouseHID.buttons = RIGHT_CLICK;
		else
		{
			mouseHID.buttons = DO_NOTHING;
			mouseRightFlag = false;
		}
	}

	//scroll w gore
	if(!mouseUpFlag && !mouseDownFlag)
		mouseHID.wheel = DO_NOTHING;
	else if(mouseUpFlag)
	{
		if(HAL_GPIO_ReadPin(JOY_UP_GPIO_Port, JOY_UP_Pin) == GPIO_PIN_SET)
			mouseHID.wheel = WHEEL_STEP;
		else
		{
			mouseHID.wheel = DO_NOTHING;
			mouseUpFlag = false;
		}
	}

	else if(mouseDownFlag)
	{
		if(HAL_GPIO_ReadPin(JOY_DOWN_GPIO_Port, JOY_DOWN_Pin) == GPIO_PIN_SET)
			mouseHID.wheel = -WHEEL_STEP;
		else
		{
			mouseHID.wheel = DO_NOTHING;
			mouseDownFlag = false;
		}
	}

	USBD_HID_SendReport(&hUsbDeviceFS, &mouseHID, sizeof(struct mouseHID_t));
}

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  file: The file name as string.
 * @param  line: The line in file as a number.
 * @retval None
 */
void _Error_Handler(char *file, int line)
{
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	while(1)
	{
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{ 
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
